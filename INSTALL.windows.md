Pingus Installation Guide for Windows
======================================

Installing through the Pingus Website
=====================================

1. Go to https://pingus.gitlab.io/
2. Select the Download tab
3. Download the most recent (0.7.6) version of the Pingus Windows Build
4. Open Pingus-0.7.6.exe on your device
5. Follow the installation prompt
6. Select "allow" to the UAC command
7. Select your desired installation repository
8. Download and finish installation of Pingus, launch the game and enjoy!

Optional: You can also run Pingus from the command line by going to the installation directory and running the command

	./pingus


Compiling from Source
======================

Requirements
------------

First, get and install cmake from https://cmake.org/download/
Version 3.24.1 is recommended.

Cloning
-----------

Clone in the desired repository of your choice using the command:

	git clone git clone https://gitlab.com/pingus/pingus.git

This command uses the HTTPS link for the pingus repository


Compilation
-----------

Once the download is complete and the repository has been cloned, you can compile the code using the command

    cmake pingus

Note: Error messages may appear depending on the cloning status of the git repository, the status of the makefile,
and the directory you desire to export the application to. Change the makefile as needed to get it to run (For example,
changing the cmake version, etc.) See INSTALL.linux.md and INSTALL.macosx.md for examples on how to alter the makefile.


Running
-------

Once the compilation is successful, you can run Pingus directly from
the Pingus directory using the command:

    ./pingus